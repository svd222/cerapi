<?php
require_once 'vendor/autoload.php';

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerBuilder;

$dotenv = Dotenv\Dotenv::createUnsafeImmutable(__DIR__);
$dotenv->load();

/**
 * @var $container ContainerBuilder
 */
$container = require_once 'bootstrap.php';

$eMB = $container->get('entityManagerBuilder');
/**
 * @var $entityManager EntityManager
 */
$entityManager = $eMB->build();

return ConsoleRunner::createHelperSet($entityManager);

