<?php
$startTime = microtime(true);
error_reporting(E_ALL);
ini_set('display_errors', 1);
//ini_set('assert.exception', 1);
//ini_set('zend.assertions', -1);
require_once './vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createUnsafeImmutable(__DIR__);
$dotenv->load();

require_once './bootstrap.php';
/**
 * @var $containerBuilder \Symfony\Component\DependencyInjection\ContainerBuilder
 */
// https://github.com/stein189/Simple-PHP-Router
use App\Component\Event;
use Szenis\Routing\Router;
use App\Component\Guard;
use App\Component\JsonResponse;
use App\Api\Controller\RateController;
use App\Component\Configuration;
use App\Component\Rule\AccessControlRule;
use App\Component\Rule\ArgumentRule;
use Szenis\Routing\Route;

$router = new Router();

$queryParams = [];
$query = parse_url($_SERVER['REQUEST_URI']);
$query = $query['query'] ?? '';
if ($query) {
    $params = explode('&', $query);
    foreach ($params as $param) {
        $param = explode('=', $param);
        $queryParams[$param[0]] = $param[1];
    }
}

$config = require_once './src/Config/main.php';
$configuration = Configuration::getInstance($config);

$controller = RateController::class;
$action = '\\'.$controller . '::runAction';
$router->add('/api/v1', 'GET|POST', $action);

$guard = new Guard();
Event::on(
    Guard::class,
    Guard::EVENT_GUARD_BEFORE_VALIDATE,
    function($event) use ($guard) {
        $guard->add(AccessControlRule::class);
        $guard->add(ArgumentRule::class);
        $event->handled = false;
        return $event;
    }, [
    'data' => $_SERVER
]);
if (!$guard->validate()) {
    echo (string)(new JsonResponse(403, null, $guard->errorMessage ?? 'Forbidden'));
    terminate();
}
Event::on(
    Guard::class,
    Guard::EVENT_GUARD_BEFORE_VALIDATE,
    function($event) {
        $event->handled = false;
        return $event;
    }, [
    'data' => $_SERVER
]);

$response = $router->resolve($_SERVER['REQUEST_URI'], $_SERVER['REQUEST_METHOD']);

switch ($response['code']) {
    case Route::STATUS_FOUND: {
        if (!isset($queryParams['method'])) {
            echo (string)(new JsonResponse(403, null, 'Requested method not exists'));
            terminate();
        }
        $controller = new $controller;
        if (($response = $controller->runAction($containerBuilder, $queryParams)) !== false) {
            echo (string)(new JsonResponse(200, $response, 'success'));
            terminate();
        } else {
            echo (string)(new JsonResponse(403, null, 'Forbidden'));
            terminate();
        }
        break;
    }
    case Route::STATUS_NOT_FOUND:
    default: {
        echo (string)(new JsonResponse(404, null, 'Page not found'));
        terminate();
        break;
    }
}

function terminate()
{
    //in simple case is a call of die() function
    die();
}
