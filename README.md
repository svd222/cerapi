##TASK 1

see sql/db.sql

##TASK 2

##INSTALLATION

~~~
$ cd /path/to/project
$ git clone https://svd222@bitbucket.org/svd222/cerapi.git .
$ composer install
$ cp .env.example .env
~~~

After that you should replace appropriate DB_* values in .env file to your own

###CREATING DB SCHEME

~~~
$ ./vendor/doctrine/orm/bin/doctrine orm:schema-tool:create
~~~

###NGINX CONFIGURATION EXAMPLE

~~~
server {
	listen 80;

	sendfile        on;
	keepalive_timeout  65;
    gzip  on;
    gzip_min_length 1024;
    gzip_buffers 12 32k;
    gzip_comp_level 9;
    gzip_proxied any;
	gzip_types	text/plain application/xml text/css text/js text/xml application/x-javascript text/javascript application/javascript application/json application/xml+rss;
	
	server_name .userapi.local;
	root /var/www/userapi;

	access_log /var/www/userapi/access.log;
	error_log /var/www/userapi/error.log;

	charset utf-8;

	location / {
	    root /var/www/userapi/;
	    index index.php;
	    try_files $uri $uri/ /index.php?$args;
	}

    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        
        root /var/www/userapi/;

        try_files $uri $uri/ =404;
        fastcgi_pass unix:/run/php/php7.3-fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param                   SCRIPT_FILENAME $document_root$fastcgi_script_name;

        fastcgi_param QUERY_STRING    $query_string;
        fastcgi_param REQUEST_METHOD  $request_method;
        fastcgi_param CONTENT_TYPE    $content_type;
        fastcgi_param CONTENT_LENGTH  $content_length;
        fastcgi_param  PATH_INFO $fastcgi_path_info;
    }
}
~~~

next goin to /etc/hosts and add this line 

~~~
127.0.0.1 userapi.local
~~~

~~~
sudo service nginx restart
~~~

after that project should be available at: http://userapi.local

###GRAB CURRENCY RATES

~~~
$ php index-cli.php -c CurrencyRateCommand/grab
~~~

~~~
API LIST
~~~

~~~
    POST /api/v1/?method=convert&currency_from=USD&currency_to=BTC&value=10
    GET /api/v1/?method=rates
    GET /api/v1/?method=rates&currency=RUB
~~~

don't forget to pass the access token, see Config/main.php group1 rule of access_control
