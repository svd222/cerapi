<?php
// bootstrap.php
use Doctrine\ORM\Tools\Setup;
use App\Component\EntityManagerBuilder;
use Symfony\Component\DependencyInjection\ContainerBuilder;

// Create Doctrine ORM configuration for Annotations
$isDevMode = true;
$proxyDir = null;
$cache = null;
$useSimpleAnnotationReader = false;
$paths = array("./src/Entity");
$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, $proxyDir, $cache, $useSimpleAnnotationReader);

// the connection configuration
$dbParams = array(
    'driver'   => getenv('db_driver'),
    'user'     => getenv('db_user'),
    'password' => getenv('db_password'),
    'host'     => getenv('db_host'),
    'dbname'   => getenv('db_name'),
);

// obtaining the entity manager
//$entityManager = EntityManager::create($dbParams, $config);
$containerBuilder = new ContainerBuilder();
$containerBuilder
    ->register('entityManagerBuilder', EntityManagerBuilder::class)
    ->addArgument($dbParams)
    ->addArgument($config);

/*$containerBuilder
    ->autowire('EntityManager', EntityManager::class)
    ->setProperty('dbParams', $dbParams)
    ->setProperty('config', $config);*/


/*$containerBuilder
    ->autowire('em', EntityManager::class)
    ->addArgument(new Reference());*/
return $containerBuilder;
