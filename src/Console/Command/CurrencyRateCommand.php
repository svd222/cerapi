<?php
namespace App\Console\Command;

use App\Component\EntityManagerBuilder;
use App\Entity\Rate;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class CurrencyRateCommand
 *
 * Provides currency rates in relation to BTC
 *
 * @package App\Console\Command
 */
class CurrencyRateCommand
{
    /**
     * Grabs currency rates and store them into DB
     * Usage: $ php index-cli.php -c CurrencyRateCommand/grab
     *
     * @param ContainerBuilder $containerBuilder
     * @param array|null $args
     */
    public function grab(ContainerBuilder $containerBuilder, ?array $args)
    {
        $providerUri = getenv('currency_provider_uri');

        $client = new Client([
            'base_uri' => $providerUri
        ]);

        /**
         * @var ResponseInterface
         */
        $res = $client->request('GET', $providerUri);

        $body = $res->getBody()->getContents();
        $rates = json_decode($body, true);

        /**
         * @var EntityManagerBuilder
         */
        $emB = $containerBuilder->get('entityManagerBuilder');

        /**
         * @var EntityManager $em
         */
        $em = $emB->build();

        $em->getConnection()->beginTransaction();

        try {
            $exist = $em->createQuery('SELECT COUNT(r) FROM ' . Rate::class . ' r')->execute();
            $exist = (bool)$exist[0][1];
            $deleted = $em->createQuery('DELETE from ' . Rate::class . ' r')->execute();
            if ($exist && !$deleted) {
                $em->getConnection()->rollBack();
                var_dump($exist);
                return false;
            }
            $batchSize = 20;
            $counter = 0;
            foreach ($rates as $key => $rate) {
                $entity = new Rate();
                $entity->setName($key);
                $entity->setM15($this->deductCommission($rate['15m']));
                $entity->setLast($this->deductCommission($rate['last']));
                $entity->setBuy($this->deductCommission($rate['buy']));
                $entity->setSell($this->deductCommission($rate['sell']));
                $entity->setSymbol($rate['symbol']);
                $em->persist($entity);
                if (($counter++ % $batchSize) === 0) {
                    $em->flush();
                }
            }
            $em->flush();
            $em->clear();

            if ($counter > 1) {
                $em->getConnection()->commit();
            }
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            var_export($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Deduct a commission
     *
     * @param $value
     * @param int $commission
     * @return float|int
     */
    protected function deductCommission($value, $commission = 2)
    {
        return $value - ($value * $commission / 100);
    }
}

/**
 * ,
"App\\Api\\Controllers\\": "src/api/controllers",
"App\\Component\\": "src/Component",
"App\\Console\\": "src/Console",
"App\\Console\\Commands\\": "src/Console/Command",
"App\\Entity\\": "src/Entity"
 */
