<?php
namespace App\Helper;

class Common
{
    public static function getQuery()
    {
        $queryParams = [];
        $query = parse_url($_SERVER['REQUEST_URI']);
        $query = $query['query'] ?? '';
        if ($query) {
            $params = explode('&', $query);
            foreach ($params as $param) {
                $param = explode('=', $param);
                $queryParams[$param[0]] = $param[1];
            }
        }
        return $queryParams;
    }
}
