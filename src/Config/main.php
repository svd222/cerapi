<?php
return [
    'components' => [
        'access_control' => [
            'group1' =>
            [
                ['type' => 'action', 'message' => 'Invalid Token', 'value1' => '%_SERVER.HTTP_AUTHORIZATION%', 'operator' => 'eq', 'value2' => 'Bearer 907c762e069589c2cd2a229cdae7b8778caa9f07f739e6abc946e718d942ee25'],
            ],
            'group2' =>
            [
                ['type' => 'condition', 'value1' => '%_SERVER.QUERY_STRING%', 'operator' => 'strContains', 'value2' => 'method=rates'],
                ['type' => 'action', 'message' => 'Invalid request method', 'value1' => '%_SERVER.REQUEST_METHOD%', 'operator' => 'eq', 'value2' => 'GET'],
            ],
            'group3' =>
            [
                ['type' => 'condition', 'value1' => '%_SERVER.QUERY_STRING%', 'operator' => 'strContains', 'value2' => 'convert'],
                ['type' => 'action', 'message' => 'Invalid request method', 'value1' => '%_SERVER.REQUEST_METHOD%', 'operator' => 'eq', 'value2' => 'POST'],
            ]
        ],
        'argument_control' => [
            [
                ['type' => 'condition', 'value1' => '%_SERVER.QUERY_STRING%', 'operator' => 'strContains', 'value2' => 'method=convert'],
                ['type' => 'action', 'message' => 'Value should be greater than or equal to 0.01', 'operator' => 'callback', 'value1' => '\App\Component\Rule\ArgumentRule::checkForMinValue', 'value2' => '%_SERVER.QUERY_STRING%'],
            ]
        ]
    ]
];
