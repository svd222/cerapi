<?php
namespace App\Interfaces;

interface IRule
{
    public function resolve(): bool;

    public function getAlias(): string;
}
