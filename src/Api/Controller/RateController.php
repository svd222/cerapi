<?php
/**
 * Created by PhpStorm
 * Author: Evgeny Berezhnoy <svd22286@gmail.com>
 * Date: 15/12/2020
 * Time: 10:27
 */

namespace App\Api\Controller;

use App\Component\EntityManagerBuilder;
use App\Entity\Rate;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use App\Component\CurrencyRate;

class RateController
{
    public function runAction(ContainerBuilder $cB, ?array $params)
    {
        header('Content-Type: application/json');
        $method = $params['method'] ?? '';
        if (method_exists($this, $method)) {
            unset($params['method']);
            return $this->$method($cB, $params);
        }
        return false;
    }

    private function getRates(ContainerBuilder $cB)
    {
        /**
         * @var EntityManagerBuilder $eMB
         */
        $eMB = $cB->get('entityManagerBuilder');
        $eM = $eMB->build();

        $btcRate = new Rate();
        $btcRate->setName('BTC');
        $btcRate->setM15(1.0);
        $btcRate->setLast(1.0);
        $btcRate->setBuy(1.0);
        $btcRate->setSell(1.0);
        $btcRate->setSymbol(1.0);

        /**
         * @var Rate[] $rates
         */
        $rates =  $eM->createQueryBuilder()
            ->select('r')
            ->from(Rate::class, 'r')
            ->where('1 = 1')
            ->orderBy('r.last', 'ASC')
            ->getQuery()
            ->getResult();

        return array_merge($rates, [$btcRate]);
    }

    private function findRateByCurrencyName(array $rates, string $currency) {
        foreach ($rates as $rate) {
            /**
             * @var Rate $rate
             */
            if ($rate->getName() == $currency) {
                return $rate;
            }
        }
        return null;
    }

    public function rates(ContainerBuilder $cB, ?array $params)
    {
        $rates = $this->getRates($cB);

        $jsonResponse = [];
        if (empty($params['currency'])) {
            /**
             * @var $eMB EntityManagerBuilder
             */
            $eMB = $cB->get('entityManagerBuilder');

            /**
             * @var $em EntityManager
             */
            $em = $eMB->build();

            $em->getConnection()->beginTransaction();
            try {
                array_walk($rates, function($rate, $key) use (&$jsonResponse) {
                    /**
                     * @var Rate $rate
                     */
                    $jsonResponse[$rate->getName()] = new CurrencyRate(
                        $rate->getName(),
                        $rate->getM15(),
                        $rate->getLast(),
                        $rate->getBuy(),
                        $rate->getSell(),
                        $rate->getSymbol()
                    );
                });
                $em->getConnection()->commit();
            } catch (\Exception $e) {
                $em->getConnection()->rollBack();
                return false;
            }
        } else {
            //find rate by name
            $rate = $this->findRateByCurrencyName($rates, $params['currency']) ?? null;
            if ($rate) {
                $jsonResponse[$rate->getName()] = new CurrencyRate(
                    $rate->getName(),
                    $rate->getM15(),
                    $rate->getLast(),
                    $rate->getBuy(),
                    $rate->getSell(),
                    $rate->getSymbol()
                );
                return $jsonResponse;
            } else {
                return false;
            }
        }

        return $jsonResponse;
    }

    public function convert(ContainerBuilder $cB, array $params)
    {
        $from = $params['currency_from'] ?? '';
        $to = $params['currency_to'] ?? '';
        $value = $params['value'] ?? 0;

        if (!$params || !$from || !$to) {
            return false;
        }

        $rates = $this->getRates($cB);
        $rateFrom = $this->findRateByCurrencyName($rates, $from);
        $rateTo = $this->findRateByCurrencyName($rates, $to);
        if (empty($rateFrom || empty($rateTo))) {
            return false;
        }
        $converted = $value * $rateTo->getLast() / $rateFrom->getLast();

        if ($from == 'BTC') {
            $converted = round($converted, 2);
        } else {
            $converted = round($converted, 10);
        }

        return new class($from, $to, $value, $converted, $converted / $value) {
            public $currency_from;

            public $currency_to;

            public $value;

            public $converted_value;

            public $rate;

            public function __construct($currency_from, $currency_to, $value, $converted_value, $rate)
            {
                $this->currency_from = $currency_from;
                $this->currency_to = $currency_to;
                $this->value = $value;
                $this->converted_value = $converted_value;
                $this->rate = $rate;
            }
        };
    }
}
