<?php
namespace App\Component;

use App\Entity\Rate;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;

final class EntityManagerBuilder
{
    /**
     * @var array
     */
    private $dbParams;

    /**
     * @var Configuration
     */
    private $config;

    public function __construct(array $dbParams, Configuration $config)
    {
        $this->dbParams = $dbParams;
        $this->config = $config;
    }

    /**
     * @return EntityManager
     * @throws \Doctrine\ORM\ORMException
     */
    public function build()
    {
        return EntityManager::create($this->dbParams, $this->config);
    }
}
