<?
/**
 * Created by PhpStorm
 * Author: Evgeny Berezhnoy <svd22286@gmail.com>
 * Date: 18/01/2021
 * Time: 18:27
 */
namespace App\Component;

use App\Component\Rule\AccessControlRule;
use App\Component\Rule\ArgumentRule;
use App\Interfaces\IRule;
use InvalidArgumentException;

/**
 * Guard is a key place to grant access to specific resource. If you need simple validate logic of piece of code
 * you should @see \App\Component\Rule\ namespace.
 *
 * Class Guard
 * @package App\Component
 */
class Guard extends Component
{
    /**
     * @const ALIASES Contains aliases and appropriate class definitions for validating rules
     * @todo Implement this const through configuration (separate it and load from config file)
     */
    const VALIDATION_ALIASES = [
        'access_control' => AccessControlRule::class,
        'argument_control' => ArgumentRule::class
    ];

    const EVENT_GUARD_BEFORE_VALIDATE = 'guard_before_validate';

    const EVENT_GUARD_AFTER_VALIDATE = 'guard_after_validate';

    /**
     * @var IRule[] $rules
     */
    protected $rules;

    /**
     * @var string $errorMessage
     */
    public $errorMessage = '';

    public function __construct()
    {
        $this->rules = [];
        parent::__construct();
    }

    public function init()
    {
        parent::init();
    }

    /**
     * @param string $ruleClassName
     * @param $data
     * @throws InvalidArgumentException
     */
    public function add(string $ruleClassName, $data = null): void
    {
        $rule = new $ruleClassName($data);
        $ruleAlias = $rule->getAlias();
        if (isset(self::VALIDATION_ALIASES[$ruleAlias])) {
            $this->rules[$ruleAlias] = $rule;
        } else {
            throw new InvalidArgumentException('Unknown validation rule');
        }
    }

    /**
     * @return bool
     */
    public function validate(): bool
    {
        Event::trigger(self::class, self::EVENT_GUARD_BEFORE_VALIDATE, new Event());
        foreach ($this->rules as $rule) {
            if (!$rule->resolve()) {
                $this->errorMessage = $rule->errorMessage;
                return false;
            }
        }
        Event::trigger(self::class, self::EVENT_GUARD_AFTER_VALIDATE, new Event());

        return true;
    }
}
