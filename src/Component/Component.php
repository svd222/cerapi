<?php
namespace App\Component;

use App\Interfaces\IEvent;

class Component
{
    protected $events;

    public function __construct()
    {
        $this->init();
    }

    protected function init()
    {

    }

    public function trigger($name, IEvent $event)
    {
        $eventHandlers = [];

        if (!empty($this->events[$name])) {
            $eventHandlers = array_merge($eventHandlers, $this->events[$name]);
        }

        if (!empty($eventHandlers)) {
            if ($event === null) {
                $event = new Event();
            }
            if ($event->sender === null) {
                $event->sender = $this;
            }
            $event->handled = false;
            $event->name = $name;
            foreach ($eventHandlers as $handler) {
                $event->data = $handler[1];
                call_user_func($handler[0], $event);
                if ($event->handled) {
                    return;
                }
            }
        }

        Event::trigger($this, $name, $event);
    }

    public function on($name, $handler, $data = null, $append = true)
    {
        if ($append || empty($this->_events[$name])) {
            $this->events[$name][] = [$handler, $data];
        } else {
            array_unshift($this->events[$name], [$handler, $data]);
        }
    }

    /**
     * @param array $config
     */
    /*public function resolveConfig(array $config) {
        $value = $config[0];
        $definition = $config[1];
        $condition = $definition[]
    }*/
}
