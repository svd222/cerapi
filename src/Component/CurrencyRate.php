<?php
namespace App\Component;

/**
 * Class CurrencyRate
 * @package App\Component
 */
class CurrencyRate
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var float
     */
    public $m15;

    /**
     * @var float
     */
    public $last;

    /**
     * @var float
     */
    public $buy;

    /**
     * @var float
     */
    public $sell;

    /**
     * @var string
     */
    public $symbol;

    public function __construct(string $name, float $m15, float $last, float  $buy, float  $sell, $symbol) {
        $this->name = $name;
        $this->m15 = $m15;
        $this->last = $last;
        $this->buy = $buy;
        $this->sell = $sell;
        $this->symbol = $symbol;
    }
}
