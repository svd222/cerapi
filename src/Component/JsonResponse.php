<?php
namespace App\Component;

/**
 * this class is a `Value Object` that represent response in a json format
 *
 * Class JsonResponse
 * @package App\Component
 */
final class JsonResponse
{
    /**
     * @var string
     */
    private $status;

    /**
     * @var int
     */
    private $code;

    /**
     * @var mixed
     */
    private $data;

    /**
     * @var string
     */
    private $message;

    private function isSuccess(): bool
    {
        return $this->code >= 200 && $this->code < 300;
    }

    public function __construct($code, $data, ?string $message)
    {
        $this->code = $code;
        $this->data = $data ?? '';
        $this->status = $this->isSuccess() ? 'success': 'error';
        $this->message = $message ?? '';
    }

    public function __toString()
    {
        $response = new \stdClass();
        $response->code = $this->code;
        $response->status = $this->status;
        if ($this->isSuccess()) {
            $response->data = $this->data;
        } else {
            $response->message = $this->message;
        }
        return json_encode($response, JSON_UNESCAPED_UNICODE);
    }
}
