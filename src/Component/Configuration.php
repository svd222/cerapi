<?php
namespace App\Component;

class Configuration
{
    private $configuration;

    private static $instance;

    private function __construct($configuration = null)
    {
        $this->configuration = $configuration;
    }

    public static function getInstance($configuration = null): self
    {
        if (empty(static::$instance)) {
            static::$instance = new self($configuration);
        }
        return static::$instance;
    }

    public function getConfig($key = '', $context = 'components')
    {
        return $key ? $this->configuration[$context][$key] ?? [] : $this->configuration[$context];
    }
}
