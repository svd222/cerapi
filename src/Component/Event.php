<?php
namespace App\Component;

use App\Interfaces\IEvent;

class Event implements IEvent
{
    /**
     * @var $sender
     */
    public $sender;

    public $data;

    public $name;

    public $handled;

    public static $events;

    public static function trigger($class, $name, $event = null)
    {
        if (empty(self::$events[$name])) {
            return;
        }

        if ($event === null) {
            $event = new static();
        }
        $event->handled = false;
        $event->name = $name;

        if (is_object($class)) {
            if ($event->sender === null) {
                $event->sender = $class;
            }
            $class = get_class($class);
        } else {
            $class = ltrim($class, '\\');
        }

        $classes = array_merge(
            [$class],
            class_parents($class, true),
            class_implements($class, true)
        );

        foreach ($classes as $class) {
            $eventHandlers = [];

            if (!empty(self::$events[$name][$class])) {
                $eventHandlers = array_merge($eventHandlers, self::$events[$name][$class]);
            }

            foreach ($eventHandlers as $handler) {
                $event->data = $handler[1];
                call_user_func($handler[0], $event);
                if ($event->handled) {
                    return;
                }
            }
        }
    }

    public static function on($class, $name, $handler, $data = null, $append = true)
    {
        $class = ltrim($class, '\\');

        if ($append || empty(self::$events[$name][$class])) {
            self::$events[$name][$class][] = [$handler, $data];
        } else {
            array_unshift(self::$events[$name][$class], [$handler, $data]);
        }
    }
}
