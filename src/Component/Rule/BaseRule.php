<?php
/**
 * Created by PhpStorm
 * Author: Evgeny Berezhnoy <svd22286@gmail.com>
 * Date: 01/18/2021
 * Time: 14:23 PM
 */
namespace App\Component\Rule;

use App\Component\Configuration;
use App\Interfaces\IRule;

/**
 * It is a base class for all rules.
 * Derived rules should be define appropriate unique alias for self (through getAlias() method) and implement resolve() method.
 * This class also defines simple methods for atomic operations like `more than` or `equal to` and other...
 * Pay attention for `callback()` operation. It is a place to define your specific logic.
 *
 * Class BaseRule
 * @package App\Component\Rule
 */
class BaseRule implements IRule
{
    /**
     * @var string $alias The alias for concrete Rule
     */
    protected $alias;

    protected $config;

    /**
     * @var string
     */
    public $errorMessage = '';

    public function __construct()
    {
        $configuration = Configuration::getInstance();
        $this->config = $configuration->getConfig(self::getAlias());
    }

    public function resolve(): bool
    {
        $resolveResult = true;
        foreach ($this->config as $groupKey => $item) {
            $conditionIsTrue = true;
            foreach ($item as $rule) {
                if ($rule['type'] == 'condition') {
                    $operator = $rule['operator'];
                    unset($rule['type'], $rule['operator']);
                    $method = $this->operatorAliases[$operator] ?? '';
                    if ($method) {
                        $rule = $this->replaceConfigValues($rule);
                        $conditionIsTrue = $this->$method(...$rule) && $conditionIsTrue;
                        if (!$conditionIsTrue) {
                            break;
                        }
                    } else {
                        throw new \InvalidArgumentException('Method with such alias `' . $operator . '` does not exists');
                    }
                }
                else if ($conditionIsTrue && $rule['type'] == 'action') {
                    $operator = $rule['operator'];
                    unset($rule['type'], $rule['operator']);
                    $message = $rule['message'] ?? '';
                    if ($message) {
                        unset($rule['message']);
                        $this->errorMessage = $message;
                    }
                    $method = $this->operatorAliases[$operator] ?? '';
                    if ($method) {
                        $rule = $this->replaceConfigValues($rule);
                        $result = $this->$method(...$rule);
                        $resolveResult = $resolveResult && $result;
                        if (!$resolveResult) {
                            return false;
                        }
                    } else {
                        throw new \InvalidArgumentException('Method with such alias `' . $operator . '` does not exists');
                    }
                }
            }
        }
        return $resolveResult;
    }

    private function replaceConfigValues(array $values): array
    {
        $newValues = [];
        foreach ($values as $value) {

            if ((substr($value, 0, 1) == '%') && (substr($value, strlen($value) - 1, 1) == '%')) {
                $value = substr($value, 1, strlen($value) - 2);

                $value = explode('.', $value);
                $var = array_shift($value);

                global ${$var};
                $var = ${$var};
                if (isset($var)) {
                    foreach ($value as $v) {
                        $var = $var[$v] ?: [];
                    }
                    array_push($newValues, $var);
                }
            } else {
                array_push($newValues, $value);
            }
        }
        return $newValues;
    }

    protected $operatorAliases = [
        'range' => 'range',
        'in' => 'in',
        'eq' => 'equalTo',
        'lt' => 'lessThan',
        'lte' => 'lessThanOrEqualTo',
        'mt' => 'moreThan',
        'mte' => 'moreThanOrEqualTo',
        'strContains' => 'stringContains',
        'callback' => 'callback'
    ];

    public function getAlias(): string
    {
        return $this->alias;
    }

    public function range($value, array $range): bool
    {
        if (count($range) != 2) {
            throw new \InvalidArgumentException();
        }

        if (!is_int($range[0]) || !is_int($range[1]) || !is_int[$value]) {
            throw new \InvalidArgumentException();
        }

        return $this->lessThanOrEqualTo($value, $range[1]) && $this->moreThanOrEqualTo($value, $range[0]);
    }

    public function stringContains(string $haystack, string $needle): bool {
        return strpos($haystack, $needle) !== false;
    }

    protected function in($actual, ...$possible): bool
    {
        return in_array($actual, $possible);
    }

    protected function equalTo($v1, $v2, $strict = false): bool
    {
        return $strict ? $v1 === $v2 : $v1 == $v2;
    }

    protected function lessThan($v1, $v2): bool
    {
        return $v1 < $v2;
    }

    protected function lessThanOrEqualTo($v1, $v2): bool
    {
        return $v1 <= $v2;
    }

    protected function moreThan($v1, $v2): bool
    {
        return $v1 > $v2;
    }

    protected function moreThanOrEqualTo($v1, $v2): bool
    {
        return $v1 >= $v2;
    }

    protected function match($v1, $expr): bool
    {
        return (bool)preg_match($expr, $v1);
    }

    protected function callback($fn, $arg): bool
    {
        if (strpos($fn, '::') !== false) {//static call
            $callable = explode('::', $fn);
            $className = $callable[0];
            $method = $callable[1];
            if (is_array($arg)) {
                return $className::$method(...$arg);
            } else {

                if (is_string($arg)) {
                    return $className::$method($arg);
                }
            }
        }
        if (function_exists($fn)) {
            if (is_array($arg)) {
                return $fn(...$arg);
            } else {
                return $fn($arg);
            }
        }
        if (method_exists($this, $fn)) {
            if (is_array($arg)) {
                return $this->$fn(...$arg);
            } else {
                return $this->$fn($arg);
            }
        }
    }
}
