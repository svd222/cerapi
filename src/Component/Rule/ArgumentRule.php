<?php
namespace App\Component\Rule;

use App\Helper\Common;

class ArgumentRule extends BaseRule
{
    protected $alias = 'argument_control';

    public static function checkForMinValue($value): bool {
        $queryParams = Common::getQuery();
        $value = $queryParams['value'] ?? 0;
        return $value >= 0.01;
    }
}
