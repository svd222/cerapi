<?php

namespace App\Component\Rule;

use App\Component\Configuration;

class AccessControlRule extends BaseRule
{
    protected $alias = 'access_control';
}
