<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="rates")
 */
class Rate
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=10)
     * @var float
     */
    protected $m15;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=10)
     * @var float
     */
    protected $last;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=10)
     * @var float
     */
    protected $buy;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=10)
     * @var float
     */
    protected $sell;

    /**
     * @ORM\Column(length=3)
     * @var string
     */
    protected $symbol;

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
    }

    public function getM15()
    {
        return $this->m15;
    }

    public function setM15($value)
    {
        $this->m15 = $value;
    }

    public function getLast()
    {
        return $this->last;
    }

    public function setLast($value)
    {
        $this->last = $value;
    }

    public function getBuy()
    {
        return $this->buy;
    }

    public function setBuy($value)
    {
        $this->buy = $value;
    }

    public function getSell()
    {
        return $this->sell;
    }

    public function setSell($value)
    {
        $this->sell = $value;
    }

    public function getSymbol()
    {
        return $this->symbol;
    }

    public function setSymbol($value)
    {
        $this->symbol = $value;
    }
}
