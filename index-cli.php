<?php
#!/usr/bin/php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once './vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createUnsafeImmutable(__DIR__);
$dotenv->load();

require_once './bootstrap.php';
/**
 * @var $containerBuilder \Symfony\Component\DependencyInjection\ContainerBuilder
 */

//here we get an arguments from standard input, parse it, next we goin to found appropriate controller/action and execute it with optional parameters as array
array_shift($argv);

if (!count($argv)) {
    throw new Exception('Empty input');
}

//get a parameters if they exists
$args = $keys = $values = [];

if (($count = count($argv))) {
    for($i = 0; $i < $count; $i++) {
        if ($i%2 == 0) {
            $key = $argv[$i];
            $key = substr($key, 0, 1) == '-' ? substr($key, 1) : '';
            if (!$key) {
                throw new InvalidArgumentException('Script argument names should be start with `-` prefix. `' . $argv[$i] . '` is incorrect');
            }
            $keys[] = $key;
        } else {
            $values[] = $argv[$i];
        }
    }
    $args = array_combine($keys, $values);
}

$controllerAction = array_shift($args);
$controllerAction = explode('/', $controllerAction);

$controller = 'App\Console\Command\\' . $controllerAction[0];
$controller = new $controller;
$action = $controllerAction[1];

$controller->$action($containerBuilder, $args);
