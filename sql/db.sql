CREATE TABLE users(
  id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  first_name VARCHAR(32) NOT NULL,
  last_name VARCHAR(32) NOT NULL,
  age TINYINT NOT NULL
);

CREATE TABLE books(
  id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` VARCHAR(64) NOT NULL,
  author VARCHAR(64) NOT NULL
);

CREATE TABLE user_books(
   id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
   user_id INTEGER NOT NULL,
   book_id INTEGER NOT NULL
);

INSERT INTO users(
    first_name, last_name, age
)
VALUES
('Ivan','Ivanov',7),
('Petr','Petrov',13),
('Marina','Tsvetaeva',15),
('Tom','Kruz',14)
;

INSERT INTO books(
    `name`, author
)
VALUES
('Romeo and Juliet', 'William Shakespeare'),
('King Lir', 'William Shakespeare'),
('Makbet','William Shakespeare'),
('War and Peace', 'Leo Tolstoy') ,
('Sunday', 'Leo Tolstoy'),
('Childhood', 'Leo Tolstoy')
;

INSERT INTO user_books (`user_id`, `book_id`)
VALUES
(1,1),
(1,3),
(2,4),
(2,5),
(3,1),
(3,2),
(3,3),
(4,1),
(4,2),
(4,4);

SELECT tt.id, tt.full_name, tt.author, tt.age, GROUP_CONCAT(tt.name SEPARATOR ', ') books FROM
    (	SELECT t.id, t.full_name, bbb.author, bbb.name, t.age
         FROM books bbb INNER JOIN user_books bu ON bbb.`id` = bu.`book_id`
                        INNER JOIN
		(
                  SELECT u.id, CONCAT(u.`first_name`, ' ', u.`last_name`) AS full_name, u.age
                  FROM users u INNER JOIN (
                      SELECT bb.`author`, ubb.`user_id` AS u_id FROM books bb
                                                                         INNER JOIN user_books ubb ON ubb.`book_id` = bb.`id`
                      GROUP BY bb.`author`,  ubb.`user_id` HAVING COUNT(bb.`author`) = 2
                               AND ubb.`user_id` NOT IN (
                              SELECT ubbb.`user_id` FROM books bbb INNER JOIN user_books ubbb ON ubbb.`book_id` = bbb.`id`
                              GROUP BY user_id HAVING COUNT(*) > 2
				)
                  ) sb ON sb.u_id = u.id
                  WHERE u.age BETWEEN 7 AND 17
              ) t ON t.id = bu.user_id
    ) tt GROUP BY tt.id, tt.full_name, tt.author, tt.age;
